-- =============================================================
-- Author: Vivian Camacho Andia | Caleb Espinoza G.
-- Create date: 27-Ago-2018
-- Description: SQL Join Types
-- Assigment: DB Training
-- Company: Avantica Technologies Bolivia
-- =============================================================

-- =============================================================
-- IMPORTANT!!!
-- 
-- All of this queries have been built on Northwind Database sample
-- If you want to import it please, use the script providing in the
-- next URL address.
-- https://gitlab.com/calebespinoza/DBTraining/blob/master/NorthwindDB/instnwnd.sql
-- =============================================================

USE Northwind;

-- SELECTS
SELECT * FROM Orders;
SELECT * FROM [Order Details];
SELECT * FROM Customers;
SELECT * FROM CustomerCustomerDemo;
SELECT * FROM EmployeeTerritories;

-- ========================
-- 1. SQL INNER JOIN
-- ========================

-- #1
-- List all of the customers who have orders displaying ContactName, Phone, Freight, OrderDate
-- RequiredDate, ShipName and the list should be ordered by ContactName
SELECT c.ContactName, c.Phone, o.Freight, o.OrderDate, o.RequiredDate, o.ShipName 
FROM Orders AS o INNER JOIN Customers AS c
ON o.CustomerID = c.CustomerID
ORDER BY c.ContactName;

-- #2
-- List all of Orders detailed with the Customer name and address, the freiht of each order.
-- In the same list all of Products displaying the product name, unit price, quantity, discount.
-- Show the employee who registered the order
-- And the total cost of each product.
SELECT o.OrderID, c.ContactName, c.Address, o.Freight, p.ProductName, p.UnitPrice, od.Quantity, od.Discount, 
e.FirstName, e.LastName, (p.UnitPrice * od.Quantity) AS Total
FROM Orders AS o 
INNER JOIN Customers AS c 
ON o.CustomerID = c.CustomerID
INNER JOIN Employees AS e
ON o.EmployeeID = e.EmployeeID
INNER JOIN [Order Details] AS od
ON o.OrderID = od.OrderID
INNER JOIN Products AS p
ON od.ProductID = p.ProductID;

-- #3
SELECT  pro.Productname, Sup.CompanyName
FROM suppliers as Sup
INNER JOIN products as pro
ON sup.SupplierID= pro. SupplierID;

-- ========================
-- 2. LEFT JOIN
-- ========================

-- #1
-- List all of customers who have or not orders registered ordered by Customer name
SELECT Customers.ContactName, Orders.OrderID
FROM Customers
LEFT JOIN Orders ON Customers.CustomerID = Orders.CustomerID
ORDER BY Customers.ContactName;

-- #2
-- List all of employees with their registered orders and its shipped date.
SELECT Employees.FirstName, Employees.LastName, Orders.OrderID, Orders.ShippedDate
FROM Employees
LEFT JOIN Orders ON Employees.EmployeeID = Orders.EmployeeID
ORDER BY Employees.FirstName;

-- #3
SELECT  pro.Productname, Sup.CompanyName
FROM suppliers as Sup
LEFT JOIN products as pro
ON sup.SupplierID= pro. SupplierID

-- #4
SELECT  pro.Productname, Sup.CompanyName
FROM suppliers as Sup
LEFT OUTER JOIN products as pro
ON sup.SupplierID= pro. SupplierID

-- ========================
-- 3. RIGHT JOIN
-- ========================

-- #1
-- List all of products with theirs categories
SELECT Products.ProductName, Categories.CategoryName
FROM Products 
RIGHT JOIN Categories
ON Categories.CategoryID = Products.CategoryID;

-- #2
-- List all of orders with theirs shippers
SELECT Orders.ShipName, Shippers.CompanyName
FROM Orders
RIGHT JOIN Shippers
ON Orders.ShipVia = Shippers.ShipperID;

-- #3
SELECT  pro.Productname, Sup.CompanyName
FROM suppliers as Sup
RIGHT JOIN products as pro
ON sup.SupplierID= pro. SupplierID

-- ========================
-- 4. FULL JOIN
-- ========================

-- #1
-- List all of products with their categories
SELECT Products.ProductName, Products.QuantityPerUnit, Suppliers.CompanyName, Suppliers.Phone 
FROM Products 
FULL JOIN Suppliers
ON Products.SupplierID = Suppliers.SupplierID;

-- #2
-- List all of employees with orders ship addresses and region.
SELECT Employees.FirstName, Employees.Region, Orders.ShipAddress, Orders.ShipRegion
FROM Employees
FULL JOIN Orders
ON Employees.EmployeeID = Orders.EmployeeID;

-- #3
SELECT  pro.Productname, Sup.CompanyName
FROM suppliers as Sup
FULL OUTER JOIN products as pro
ON sup.SupplierID= pro. SupplierID

-- =======================================
-- 5. LEFT JOIN (sans l'intersection de B)
-- =======================================

-- #1
-- 
SELECT Customers.ContactName, Orders.OrderID
FROM Customers
LEFT JOIN Orders ON Customers.CustomerID = Orders.CustomerID
WHERE Orders.CustomerID IS NULL
ORDER BY Customers.ContactName;

-- #2
-- 
SELECT Employees.FirstName, Employees.LastName, Orders.OrderID, Orders.ShippedDate
FROM Employees
LEFT JOIN Orders ON Employees.EmployeeID = Orders.EmployeeID
WHERE Orders.EmployeeID IS NULL
ORDER BY Employees.FirstName;

-- #3
SELECT  pro.Productname, Sup.CompanyName
FROM suppliers as Sup
FULL OUTER JOIN products as pro
ON sup.SupplierID= pro. SupplierID
WHERE Sup.SupplierID IS NULL OR pro. SupplierID IS NULL

-- ========================================
-- 6. RIGHT JOIN (sans l'intersection de A)
-- ========================================

-- #1
-- List all of products with theirs categories
SELECT Products.ProductName, Categories.CategoryName
FROM Products 
RIGHT JOIN Categories
ON Categories.CategoryID = Products.CategoryID
WHERE Categories.CategoryID IS NULL;

-- #2
-- List all of orders with theirs shippers
SELECT Orders.ShipName, Shippers.CompanyName
FROM Orders
RIGHT JOIN Shippers
ON Orders.ShipVia = Shippers.ShipperID
WHERE Orders.ShipVia IS NULL;

-- #3
SELECT  pro.Productname, Sup.CompanyName
FROM suppliers as Sup
LEFT OUTER JOIN products as pro
ON sup.SupplierID= pro. SupplierID
WHERE pro.SupplierID IS null

-- =================================
-- 7. FULL JOIN (sans intersection)
-- =================================

-- #1
-- List all of products with their categories
SELECT Products.ProductName, Products.QuantityPerUnit, Suppliers.CompanyName, Suppliers.Phone 
FROM Products 
FULL JOIN Suppliers
ON Products.SupplierID = Suppliers.SupplierID
WHERE Products.SupplierID IS NULL
OR Suppliers.SupplierID IS NULL;

-- #2
-- List all of employees with orders ship addresses and region.
SELECT Employees.FirstName, Employees.Region, Orders.ShipAddress, Orders.ShipRegion
FROM Employees
FULL JOIN Orders
ON Employees.EmployeeID = Orders.EmployeeID
WHERE Employees.EmployeeID IS NULL
OR Orders.EmployeeID IS NULL;

-- ========================
-- 8. SQL CROSS JOIN
-- ========================

-- #1
SELECT Customers.ContactName, Orders.OrderDate, Orders.RequiredDate
FROM Customers CROSS JOIN Orders;

-- #2
SELECT Products.ProductName, Products.ReorderLevel, Shippers.CompanyName
FROM Products CROSS JOIN Shippers
ORDER BY Products.ReorderLevel;

-- #3
SELECT  pro.Productname, Sup.CompanyName
FROM suppliers as Sup
CROSS JOIN products as pro

--#4
SELECT * 
FROM Persona
CROSS JOIN Libro
