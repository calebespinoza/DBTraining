/*
    Name: Caleb Espinoza G.
    Practice 1
    DB Training
    Avantica Technologies Bolivia
*/

-- #1
-- Create a new database called 'StoreDB'

CREATE DATABASE StoreDB
GO

-- #2
-- Drop the database 'StoreDB'
DROP DATABASE StoreDB
GO

-- Connect to the 'StoreDBr' database to run this snippet
USE StoreDB
GO

-- #3
-- Create a new table called 'Productos'
CREATE TABLE Productos
(
    CodProducto INT NOT NULL PRIMARY KEY, -- primary key column
    Nombre VARCHAR(50) NOT NULL,
    Precio MONEY NOT NULL
);
GO

-- Create a new table called 'Clientes'
CREATE TABLE Clientes
(
    Id INT NOT NULL PRIMARY KEY,
    Nombre VARCHAR(50) NULL,
    Apellido VARCHAR(50) NULL,
    Edad INT NULL
);
GO

-- Create a new table called 'Ventas'
CREATE TABLE Ventas
(
    Id INT NOT NULL PRIMARY KEY,
    IdProducto INT NOT NULL,
    IdCliente INT NOT NULL,
    Fecha DATE NULL,
    CONSTRAINT FK_Producto_Ventas 
    FOREIGN KEY (IdProducto) 
    REFERENCES Productos(CodProducto)
    ON DELETE CASCADE,
    CONSTRAINT FK_Clientes_Ventas
    FOREIGN KEY (IdCliente)
    REFERENCES Clientes(Id)
    ON DELETE CASCADE
);
GO

-- Get a list of tables and views in the current database
--SELECT table_name name, table_type type
--FROM INFORMATION_SCHEMA.TABLES
--GO

-- #4
-- Drop the table 'Productos'
DROP TABLE Productos
GO
-- Drop the table 'Productos'
DROP TABLE Clientes
GO
-- Drop the table 'Productos'
DROP TABLE Ventas
GO

-- #5
-- Modify name of column "Apellido" for "Apellidos"
EXEC sp_rename  'StoreDB.Cliente.Apellido', 'Apellidos', 'COLUMN';

-- #6
TRUNCATE TABLE Ventas
GO

-- #7
-- Insert rows into table 'Clientes'
INSERT INTO Clientes VALUES(1, 'Peter', 'Hollens', 40)
INSERT INTO Clientes VALUES(2, 'Ed', 'Sheeran', 31)
INSERT INTO Clientes VALUES(3, 'Lindsay', 'Starling', NULL)
INSERT INTO Clientes VALUES(4, 'Jhon', 'Legend', NULL)
INSERT INTO Clientes VALUES(5, 'Thomas', 'Bangalter', NULL)

-- Insert rows into table 'Productos'
INSERT INTO Productos VALUES(1, 'Macbook Air 13 inch', 2100)
INSERT INTO Productos VALUES(2, 'Samsung Galaxy S9 Edge', 870)
INSERT INTO Productos VALUES(3, 'Huawei P20', 1000)
INSERT INTO Productos VALUES(4, 'iPhone X', 1500)
INSERT INTO Productos VALUES(5, 'Dell Latitude E7200', 1700)

-- Insert rows into table 'Ventas'
INSERT INTO Ventas
VALUES
(1, 2, 1, GETDATE()),
(2, 4, 4, GETDATE()),
(3, 5, 2, GETDATE()),
(4, 1, 3, GETDATE()),
(5, 3, 5, GETDATE())
GO

-- #8
-- List all sells
SELECT Ventas.Id, Clientes.Nombre AS Cliente, Productos.Nombre AS Producto 
FROM Ventas
INNER JOIN Clientes ON Clientes.Id = Ventas.IdCliente 
INNER JOIN Productos ON Productos.CodProducto = Ventas.IdProducto
GO

-- #9
-- List all first 10 clients
SELECT TOP 10 * FROM Clientes
GO

-- #10
-- List clients grouped by age
SELECT COUNT(Id), Edad FROM Clientes
GROUP BY Clientes.Edad

-- #11
-- List all clients with age > 20
SELECT * FROM Clientes
WHERE Clientes.Edad > 20

-- #12
-- Remove all clients who have age > 20
DELETE Clientes WHERE Clientes.Edad > 20
