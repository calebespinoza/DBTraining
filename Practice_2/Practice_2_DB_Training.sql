CREATE DATABASE LibraryDB;

USE LibraryDB;

CREATE TABLE Persona(
	persona_id SMALLINT PRIMARY KEY NOT NULL,
	nombre VARCHAR(20) NOT NULL,
	apellido VARCHAR(20) NULL,
	genero CHAR(1) NOT NULL,
	fechaNacimiento DATE NULL,
	pais VARCHAR(20) NULL
);
CREATE TABLE Libro(
	libro_id SMALLINT NOT NULL,
	titulo VARCHAR(50) NOT NULL,
	precio DECIMAL(5,2) NULL,
	persona_id SMALLINT NOT NULL,
	PRIMARY KEY (libro_id),
	CONSTRAINT FK_Persona_Libro
	FOREIGN KEY(persona_id) REFERENCES Persona (persona_id)
);

-- #1
-- Register 7 people with full data
INSERT INTO Persona 
VALUES	(1, 'Caleb', 'Espinoza Gutierrez',	'F', '1989-02-05', 'Bolivia'),
		(2, 'Patrik Gerson', 'Delgadillo Machado',	'M', '1981-08-08', 'Argentina'),
		(3, 'Jaime', 'Rocha Subieta', 'M', '1985-09-05', 'Bolivia'),
		(4, 'Franklin', 'Cayo Flores Tola',	'M', '1985-01-29', 'EEUU'),
		(5, 'Daniel', 'Merida Bustamante', 'M', '1992-10-21', 'EEUU'),
		(6, 'Patricia', 'Peredo D�valos',	'F', '1980-09-21', 'PERU'),
		(7, 'Claudia', 'Villaroel Alborta', 'F', '1983-02-14', 'CHILE');

-- #2
-- Register 3 people without nacionality
INSERT INTO Persona 
VALUES	(8, 'Juan Carlos', 'Pinto Martinez','M', '1980-07-15', NULL),
		(9, 'Areliez Katrin', 'Vargas', 'F', '1991-09-12', NULL),
		(10, 'Maira', 'Huarachi', 'F', '1991-01-01', NULL);

-- #3
-- Register 1 person without nacionality and birthday
INSERT INTO Persona 
VALUES	(11, 'Wilson', 'Mamani Flores', 'M', NULL, NULL);

--#4
-- Register 10 books
INSERT INTO Libro
VALUES (1, 'Ready Player One', 250, 1),
(2, 'A Street Cat Named Bob', 210, 8),
(3, 'Fred Factor', 150, 5),
(4, 'Innovation is Everybody Bussines', 180, 7),
(5, '5 Disfunctions of a Team', 120, 9),
(6, 'The Book Thief', 200, 2),
(7, 'Ana Frank Diary', 180, 11),
(8, 'Steve Jobs Biography', 250, 10),
(9, 'The Fellowship of The Ring', 300, 7),
(10, 'A Storm of Swords', 130, 8);

-- #5
-- List name and last name from Persona table order by Apellido and then by name
SELECT Persona.nombre, Persona.apellido FROM dbo.Persona ORDER BY 1, 2;

-- #6
-- List the books with price more than 50 and the book title begin with A letter
SELECT Libro.titulo, Libro.precio FROM dbo.Libro 
WHERE LEFT(Libro.titulo, 1)='A' AND Libro.precio > 50;

-- #7
-- List name and last name of people that have at least a book with price greater than 50
SELECT Persona.nombre, Persona.apellido FROM dbo.Persona 
WHERE Persona.persona_id IN ( SELECT Libro.persona_id FROM Libro WHERE Libro.precio > 50);

-- #8
-- List name and last name of people that born at year 1990 
SELECT Persona.nombre, Persona.apellido FROM dbo.Persona
WHERE Persona.fechaNacimiento LIKE '1990_%';

-- #9
-- List Books where the book title contains 'e' letter in the second position
SELECT * FROM dbo.Libro WHERE Libro.titulo LIKE '_e%';

-- #10
-- Count the number of registries from Persona table
SELECT count(*) FROM dbo.Libro;

-- #11
-- List cheaper books
SELECT MIN (Libro.precio) Barato FROM dbo.Libro;

-- #12
-- List most expensive books
SELECT MAX (Libro.precio) Caro FROM dbo.Libro;

-- #13
-- Show the books price average
SELECT AVG(Libro.precio) FROM dbo.Libro;

-- $14
-- Show the sum of books prices
SELECT SUM(Libro.precio) FROM dbo.Libro;

-- #15
-- Modify the lenght of Titulo field in order to support until 300 characters
ALTER TABLE dbo.Libro
ALTER COLUMN titulo VARCHAR(300);

-- #16
-- List all people who are from Colombia or Mexico country
SELECT * FROM dbo.Persona WHERE Persona.pais = 'Colombia' OR Persona.pais = 'Mexico';

-- #17
-- List all books that their titles don't begin with T letter
SELECT * FROM dbo.Libro WHERE NOT LEFT(Libro.titulo, 1)='T';

-- #18
-- List all books that have a price between 50 to 70
SELECT * FROM dbo.Libro WHERE precio BETWEEN 50 AND 70

-- #19
-- List all people GROUPED by country
SELECT Persona.pais, COUNT(*) as Cantidad 
FROM dbo.Persona 
GROUP BY Persona.pais;

-- #20
-- List all people GROUPED by country and genre
SELECT Persona.pais, Persona.genero, COUNT(*) as Cantidad 
FROM dbo.Persona 
GROUP BY Persona.pais, Persona.genero;

-- #21
-- List all people GROUPED by genre and the quantity be more than 5
SELECT Persona.genero, COUNT(*) as Cantidad 
FROM dbo.Persona 
GROUP BY Persona.genero
HAVING COUNT(Persona.persona_id) > 5;

-- #22
-- List the books ORDER by title in Descendent
SELECT * FROM dbo.Libro ORDER BY Libro.titulo DESC;

-- #23
-- Delete book with title "SQL Server" 
DELETE FROM dbo.Libro WHERE Libro.titulo = 'SQL Server';

-- #24
-- Delete the books which their titles begin with "A" letter
DELETE FROM dbo.Libro WHERE LEFT(Libro.titulo, 1) = 'A';

-- #25
-- Delete the people who don't have books
DELETE FROM dbo.Persona WHERE Persona.persona_id 
NOT IN (SELECT DISTINCT Libro.persona_id FROM dbo.Libro);

-- #26
-- Delete all books
DELETE FROM dbo.Libro;

-- #27
-- Modify the person name called 'Marco' for 'Marcos'
UPDATE dbo.Persona SET Persona.nombre = 'Marcos' WHERE Persona.nombre = 'Marco';

-- #28
-- Modify the price of all books that begin with the word 'Calculo' to 57
UPDATE dbo.Libro SET Libro.precio = 57 WHERE Libro.titulo LIKE 'C[a|�]lculo_%';

-- #29
-- Show the prices list of all books grouped by price and the quantity of books with the same price
SELECT Libro.precio, COUNT(*) as Cantidad 
FROM dbo.Libro
GROUP BY Libro.precio
HAVING COUNT(libro.libro_id) > 0;