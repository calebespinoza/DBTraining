-- =============================================================
-- Author: Caleb Espinoza G.
-- Create date: 31-Ago-2018
-- Description: SQL Views and Stored Procedures
-- Assigment: DB Training
-- Company: Avantica Technologies Bolivia
-- =============================================================

-- =============================================================
-- IMPORTANT!!!
-- 
-- All of this queries have been built on Northwind Database sample
-- If you want to import it please, use the script providing in the
-- next URL address.
-- https://gitlab.com/calebespinoza/DBTraining/blob/master/NorthwindDB/instnwnd.sql
-- =============================================================

USE Northwind;
-- ============================================================
-- SQL VIEWS
-- ============================================================

-- #1
-- List all of employees with their registered orders and its shipped date.

CREATE VIEW employees_with_orders_vw AS
SELECT Employees.FirstName, Employees.LastName, Orders.OrderID, Orders.ShippedDate
FROM Employees
LEFT JOIN Orders ON Employees.EmployeeID = Orders.EmployeeID;
-- The ORDER BY clause is invalid in views
-- ORDER BY Employees.FirstName;

SELECT * FROM employees_with_orders_vw;

-- #2
-- List all Products with prices supplied by Norske Meierier company
CREATE VIEW products_from_norske_meierier_vw AS
SELECT Products.ProductName, Products.UnitPrice FROM Products
JOIN Suppliers
ON Products.SupplierID = Suppliers.SupplierID
WHERE Suppliers.CompanyName = 'Norske Meierier';

SELECT * FROM products_from_norske_meierier_vw;

-- #3
-- List all of products with Beberages category
CREATE VIEW [all Product From Beverage Category vw] AS
SELECT Products.ProductName, Categories.CategoryName
FROM Products 
RIGHT JOIN Categories
ON Categories.CategoryID = Products.CategoryID
WHERE Categories.CategoryName = 'Beverages';

-- ============================================================
-- SQL STORED PROCEDURES
-- ============================================================

-- #1 
-- Find a Employee by lastname
CREATE PROCEDURE Find_Employees_by_LastName @lastname nvarchar(20) AS
SELECT Employees.FirstName, Employees.LastName
FROM Employees
WHERE Employees.LastName = @lastname;

EXEC Find_Employees_by_LastName @lastname = 'Buchanan';
EXEC Find_Employees_by_LastName @lastname = 'Dodsworth';

-- #2 
-- Find a Customer and his/her phone by contactname
CREATE PROCEDURE Find_Customer_by_LastName @contactname nvarchar(20) AS
SELECT Customers.ContactName, Customers.Phone
FROM Customers
WHERE Customers.ContactName = @contactname;

EXEC Find_Customer_by_LastName @contactname = 'Matti Karttunen';

-- #3
-- List all customers by country
CREATE PROCEDURE Customer_by_Country @country nvarchar(20) AS
SELECT Customers.ContactName, Customers.Phone, Customers.Country
FROM Customers
WHERE Customers.Country = @country;

EXEC Customer_by_Country @country = 'Germany';
EXEC Customer_by_Country @country = 'Mexico';

-- #4
-- Show products by the country of supplier
CREATE PROCEDURE Products_by_Suppliers_Country @country nvarchar(20) AS
SELECT Products.ProductName, Products.UnitPrice, Suppliers.Country
FROM Products JOIN Suppliers
ON Products.SupplierID = Suppliers.SupplierID
WHERE Suppliers.Country = @country;

EXEC Products_by_Suppliers_Country @country = 'Germany';

-- #5
-- Show orders by customer
CREATE PROCEDURE Orders_by_Customer @contactname nvarchar(20) AS
SELECT Orders.OrderID, Orders.OrderDate, Orders.Freight, Customers.ContactName
FROM Orders JOIN Customers
ON Orders.CustomerID = Customers.CustomerID
WHERE Customers.ContactName = @contactname;

EXEC Orders_by_Customer @contactname = 'Mario Pontes';