-- =============================================================
-- Author: Caleb Espinoza G.
-- Create date: 03-Sep-2018
-- Description: SQL Research and SQL Functions
-- Assigment: DB Training
-- Company: Avantica Technologies Bolivia
-- =============================================================

-- ==================================================
-- RESEARCH
-- ==================================================

/* 
   1. is NOT NULL expression a mandatory to use with PRIMARY KEY?
   R. No, when you use PRIMARY KEY expression in a CREATE TABLE statement is not a mandatory, SQL doesn't allow to register data into primary key column by default.
   But, if you don't use AUTO INCREMENT expression, the rows will not increment automatically.
*/

create table mytable (
  col1 int primary key,
  col2 int,
  col3 int not null
);

select table_name, column_name, IS_NULLABLE
from information_schema.columns where table_name = 'MYTABLE';

/*

TABLE_NAME                     COLUMN_NAME                    IS_NULLABLE
------------------------------ ------------------------------ --------
MYTABLE                        COL1                           NO        
MYTABLE                        COL2                           YES        
MYTABLE                        COL3                           NO 

*/

/* 
	2. When is recommendable to use the Money and Decimal data types?
	R. 
		* decimal(p,s)
		  Fixed precision and scale numbers.
		  Allows numbers from -10^38 +1 to 10^38 �1.
		  The p parameter indicates the maximum total number of digits that can be stored (both to the left and to the right of the decimal point). 
		  p must be a value from 1 to 38. Default is 18.
		  The s parameter indicates the maximum number of digits stored to the right of the decimal point. s must be a value from 0 to p. Default value is 0.

		* money
		  Monetary data from -922,337,203,685,477.5808 to 922,337,203,685,477.5807

		There are a lot of people in blogs saying money is inexact and is good as long as you don't need more than 4 decimal digits
*/

DECLARE
@mon1 MONEY,
@mon4 MONEY,
@num1 DECIMAL(19,8),
@num2 DECIMAL(19,8),
@num3 DECIMAL(19,8),
@num4 DECIMAL(19,8)

SELECT
@mon1 = 100,
@num1 = 100, @num2 = 339, @num3 = 10000

SET @mon4 = @mon1/@num2*@num3
SET @num4 = @num1/@num2*@num3

SELECT @mon4 AS moneyresult,
@num4 AS numericresult

/*
	3. Is there any differences between the Decimal and Numeric data type?
	R. Numeric data types that have fixed precision and scale. Decimal and numeric are synonyms and can be used interchangeably.
	The difference between NUMERIC and DECIMAL is subtle. NUMERIC specifies the exact precision and scale to be used. 
	DECIMAL specifies the exact scale, but the precision is implementation-defined to be equal to or greater than the specified value. 
	It is a subtle difference, but T-SQL handles them the same way; DECIMAL is therefore the preferred choice for T-SQL and portability.
*/

USE Northwind;

-- ==================================================
-- SQL FUNCTIONS
-- ==================================================

SELECT SESSION_USER;
SELECT SYSTEM_USER;
SELECT CURRENT_USER;
SELECT GETDATE();
SELECT CURRENT_TIMESTAMP;

/*
	4. Develop 3 SQL Server FUNCTIONS 
*/

-- #1 Add two numbers
CREATE FUNCTION AddTwoNumbers(@int1 as int, @int2 as int)
RETURNS int
AS
BEGIN
RETURN (@int1 + @int2)
END

SELECT dbo.AddTwoNumbers(10,12);

-- #2 Get the quantity of orders at the same date
CREATE FUNCTION QuantityOrders (@OrderDate datetime)
RETURNS int
AS

BEGIN
	DECLARE @cant int;
	SELECT @cant = count(Orders.OrderID)
	FROM dbo.Orders
	WHERE Orders.OrderDate = @OrderDate
RETURN @cant
END;

SELECT dbo.QuantityOrders('1996-12-27') AS [Quantity]

-- #3 Get the customer's detailed list from a order
CREATE FUNCTION getTable(@orderID int)
RETURNS table
AS

RETURN (
	SELECT o.OrderID, c.ContactName, c.Address, o.Freight, p.ProductName, p.UnitPrice, od.Quantity, od.Discount, 
	e.FirstName, e.LastName, (p.UnitPrice * od.Quantity) AS Total
	FROM Orders AS o 
	INNER JOIN Customers AS c 
	ON o.CustomerID = c.CustomerID
	INNER JOIN Employees AS e
	ON o.EmployeeID = e.EmployeeID
	INNER JOIN [Order Details] AS od
	ON o.OrderID = od.OrderID
	INNER JOIN Products AS p
	ON od.ProductID = p.ProductID
	WHERE o.OrderID = @orderID
);

SELECT * from dbo.getTable(10248);